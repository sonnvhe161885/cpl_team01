import { getHeaders } from "../utils/headers.util";

export async function getArticles(
  { offset, author, tag, favorited, limit },
  signal
) {
  const params = new URLSearchParams({
    limit: (limit || "10").toString(),
    offset: (offset || "0").toString(),
    ...(author ? { author } : {}),
    ...(tag ? { tag } : {}),
    ...(favorited ? { favorited } : {}),
  });
  return fetch("https://api.realworld.io/api/articles?" + params, {
    headers: getHeaders(),
    signal,
  }).then((res) => res.json());
}

export async function getPersonalFeed({ offset, limit }, signal) {
  const params = new URLSearchParams({
    limit: (limit || "10").toString(),
    offset: (offset || "0").toString(),
  });
  return fetch("https://api.realworld.io/api/articles/feed?" + params, {
    headers: getHeaders(),
    signal,
  }).then((res) => res.json());
}

export async function getArticle(slug) {
  return fetch(`https://api.realworld.io/api/articles/${slug}`, {
    headers: getHeaders(),
  })
    .then((res) => res.json())
    .then((res) => res.article);
}

export async function createArticle(article) {
  return fetch("https://api.realworld.io/api/articles", {
    method: "POST",
    headers: getHeaders(),
    body: JSON.stringify({ article }),
    
  });
  // console.log(he)
}

export async function updateArticle(slug, article) {
  return fetch(`https://api.realworld.io/api/articles/${slug}`, {
    method: "PUT",
    headers: getHeaders(),
    body: JSON.stringify({ article }),
  });
}

export async function deleteArticle(slug) {
  return fetch(`https://api.realworld.io/api/articles/${slug}`, {
    method: "DELETE",
    headers: getHeaders(),
  });
}

export async function favoriteArticle(slug) {
  return fetch(`https://api.realworld.io/api/articles/${slug}/favorite`, {
    method: "POST",
    headers: getHeaders(),
  })
    .then((res) => res.json())
    .then((res) => res.article);
}

export async function unfavoriteArticle(slug) {
  return fetch(`https://api.realworld.io/api/articles/${slug}/favorite`, {
    method: "DELETE",
    headers: getHeaders(),
  })
    .then((res) => res.json())
    .then((res) => res.article);
}
