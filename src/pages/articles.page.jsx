import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import FormattedDate from "../components/formatted-date";
import { getArticle } from "../services/articles.service";
import TagList from "../components/tag-list";
import Markup from "../components/marked";
import ArticleActions from "../components/acticles-action.component";

const articleQuery = (slug) => ({
  queryKey: ["article", slug],
  queryFn: () => getArticle(slug),
});

export const loader = (queryClient) => async (slug) => {
  const query = articleQuery(slug);
  return await queryClient.ensureQueryData(query);
};

export default function ArticlePage() {
  const { slug } = useParams();
  const [article, setArticle] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    async function main() {
      const res = await getArticle(slug);
      if (res === undefined) return 1;
      setArticle(res);
    }
    main();
  }, []);

  return (
    article && (
      <div className="article-page">
        <div className="banner" style={{ backgroundColor: "rgb(105 87 87)" }}>
          <div className="container" style={{ margin: "0 auto", padding: "0 15px" }}>
            <h1 style={{ color: "#ffffff" }}>{article?.title}</h1>

            <div className="article-meta" style={{
              display: "block",
              position: "relative",
              fontWeight: 300,
              margin: "2rem 0 0",
            }}>
              <Link to={"/profile/" + article?.author?.username}>
                <img src={article?.author?.image} alt="author avatar" style={{
                  display: "inline-block",
                  verticalAlign: "middle",
                  height: "32px",
                  width: "32px",
                  borderRadius: "30px",
                }} />
              </Link>
              <div className="info">
                <Link
                  to={"/profile/" + article?.author?.username}
                  className="author"
                >
                  {article?.author?.username}
                </Link>
                <FormattedDate date={article?.createdAt} style={{ color: "#bbb", fontSize: ".8rem", display: "block" }} />
              </div>
              <ArticleActions article={article} />
            </div>
          </div>
        </div>

        <div className="container page" style={{ marginLeft: "auto", marginRight: "auto", paddingLeft: "15px", paddingRight: "15px" }}>
          <div className="row article-content">
            <div className="col-xs-12">
              <p>{article?.description}</p>
              <Markup content={article.body} />
              <TagList tags={article?.tagList} />
            </div>
          </div>

          <hr />

        </div>
      </div>
    )
  );
}
