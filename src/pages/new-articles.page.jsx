import React, { useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';


import {
  getArticle,
  createArticle,
  updateArticle,
} from "../services/articles.service";
import { useNavigate, useParams } from 'react-router';

function EditorPage({ match }) {
  const { slug } = useParams();
  // const slug = "dasdasdasd123123-20186"
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [body, setBody] = useState('');
  const [tagInput, setTagInput] = useState('');
  const [tagList, setTagList] = useState([]);
  const [article, setArticle] = useState({});
  const navigate = useNavigate();


  useEffect(() => {
  const fetcharticle = async () => {
    try{
      const a = await getArticle(slug);
      setArticle(a);
    }catch (error) {
      console.error('Error fetching article:', error);
    }
  
  };
  fetcharticle();
}, []);
  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  const changeDescription = (event) => {
    setDescription(event.target.value);
  };

  const changeBody = (event) => {
    setBody(event.target.value);
  };

  const changeTagInput = (event) => {
    setTagInput(event.target.value);
  };

  const reset = () => {
    if (slug && article) {
      setTitle(article.title);
      setDescription(article.description);
      setBody(article.body);
      setTagList(article.tagList);
    } else {
      setTitle('');
      setDescription('');
      setBody('');
      setTagInput('');
      setTagList([]);
    }
  };

  const addTag = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();

      if (tagInput && !tagList.includes(tagInput))
        setTagList([...tagList, tagInput]);

      setTagInput('');
    }
  };

  const removeTag = (tag) => () => {
    setTagList(tagList.filter((_tag) => _tag !== tag));
  };

  async function submitForm(event) {
    event.preventDefault();

    const credentials = {
      slug,
      title,
      description,
      body,
      tagList,
    };

    const promise = slug
      ? () => updateArticle(slug, credentials)
      : () => createArticle(credentials);

    const response = await promise();

    if (response.ok) {
      const { article } = await response.json();
      
      navigate(`/articles/${article.slug}`);
    }
  };

  return (
    <div className="editor-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-10 offset-md-1 col-xs-12">
            {/* <ListErrors errors={errors} /> */}

            <form>
              <fieldset>
                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Article Title"
                    value={title}
                    onChange={changeTitle}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="What's this article about?"
                    value={description}
                    onChange={changeDescription}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <textarea
                    className="form-control"
                    rows="8"
                    placeholder="Write your article (in markdown)"
                    value={body}
                    onChange={changeBody}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Enter tags"
                    value={tagInput}
                    onChange={changeTagInput}
                    onKeyUp={addTag}
                  />

                  <div className="tag-list">
                    {tagList.map((tag) => {
                      return (
                        <span className="tag-default tag-pill" key={tag}>
                          <i
                            className="ion-close-round"
                            onClick={removeTag(tag)}
                          />
                          {tag}
                        </span>
                      );
                    })}
                  </div>
                </fieldset>

                <button
                  className="btn btn-lg pull-xs-right btn-primary"
                  type="button"
                  // disabled={inProgress}
                  onClick={submitForm}
                >
                  Publish Article
                </button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditorPage;
