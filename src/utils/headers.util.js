import {getToken} from './storages.util';

export function getHeaders() {
  const token = getToken();
  console.log(token);
  return {
    'Content-Type': 'application/json',
    ...(token ? { 'Authorization': `Token ${token}` } : {}),
  };
  
}