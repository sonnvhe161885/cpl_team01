import axios from "axios";

const TOKEN_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoyMDMxOH0sImlhdCI6MTcxMzEyNjAyNywiZXhwIjoxNzE4MzEwMDI3fQ.grSQLvBb_jSiob7l_q6_EACJns433cD5mQ7com_tITI";

export function getToken() {
  const token = localStorage.getItem(TOKEN_KEY);
  console.log(token);
  if (token && token.trim() !== "") {
    return token;
  } else {
    return TOKEN_KEY; 
  }
}

export function setToken(token) {
  localStorage.setItem(TOKEN_KEY, token);
  // console.log(token);
}

export function clearToken() {
  localStorage.removeItem(TOKEN_KEY);
}