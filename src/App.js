import React from 'react';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Login from './components/Login';
import Register from './components/Register';
import ArticleDetail from './components/ArticleDetail';
import UpdateProfile from './components/UpdateProfile';
import Articles from './components/Articles';

import  EditorPage from './pages/new-articles.page';
import ArticlePage, { loader as articleLoader } from './pages/articles.page';

function App() {
  return (
    <div className="app">
      <Header />

      <Routes>
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route path="/" element={<Articles />} />
        <Route path="/article/:slug" element={<ArticleDetail />} />
        <Route path="/editor" element={<EditorPage />}/>
        <Route path="/editor/:slug" element={<EditorPage />} />
        <Route path="articles/:slug" element={<ArticlePage />} />
        <Route path="/settings" element={<UpdateProfile/>} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
