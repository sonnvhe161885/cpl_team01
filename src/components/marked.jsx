import { marked } from "marked";

export default function Markup({ content }) {
  const markup = {
    __html: marked(content, {
      breaks: true,
      gfm: true,
      mangle: false,
      headerIds: false,
    }),
  };

  return <div dangerouslySetInnerHTML={markup}></div>;
}
