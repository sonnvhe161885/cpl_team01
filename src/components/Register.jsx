import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useNavigate, Link } from "react-router-dom";

import axios from "axios";

function Register() {
  const [errors, setErrors] = useState({});
  const nav = useNavigate();

  const validationSchema = Yup.object().shape({
    username: Yup.string().required("Username is required"),
    email: Yup.string().email("Email is invalid").required("Email is required"),
    password: Yup.string().required("Password is required"),
  });

  const handleRegister = async (values) => {
    const user = {
      user: {
        username: values.username,
        email: values.email,
        password: values.password,
      },
    };

    try {
      console.log(user);
      const res = await axios.post("https://api.realworld.io/api/users", user);
      console.log(res.data.user); // You can handle successful registration here
      alert("Registration successful! You can now log in.");
      nav("/login"); // Navigate to the login page after successful registration
    } catch (error) {
      if (error.res && error.res.status === 422) {
        // Handle validation errors from the API
        error.res.data.errors.forEach(({ field, message }) => {
          setErrors({ ...errors, [field]: message });
        });
      } else {
        // Handle other errors
        alert("An error occurred while registering. Please try again later.");
      }
    }
  };

  return (
    <div>
      <div className="auth-page">
        <div className="container page">
          <div className="row">
            <div className="col-md-6 offset-md-3 col-xs-12">
              <h1 className="text-xs-center text-center">Sign up</h1>
              <p className="text-xs-center text-center">
                <Link to="/login">Have an account?</Link>
              </p>
              <Formik
                initialValues={{ username: "", email: "", password: "" }}
                onSubmit={(values) => handleRegister(values)}
                validationSchema={validationSchema}
              >
                <Form>
                  <ErrorMessage
                    className="text-center text-danger"
                    name="username"
                    component={"div"}
                  />
                  <ErrorMessage
                    className="text-center text-danger"
                    name="email"
                    component={"div"}
                  />
                  <ErrorMessage
                    className="text-center text-danger"
                    name="password"
                    component={"div"}
                  />
                  <Field
                    className="form-control form-control-lg"
                    name="username"
                    type="text"
                    placeholder="Username"
                  />
                  <br />
                  <Field
                    className="form-control form-control-lg"
                    name="email"
                    type="email"
                    placeholder="Email"
                  />
                  <br />
                  <Field
                    className="form-control form-control-lg"
                    name="password"
                    type="password"
                    placeholder="Password"
                  />
                  <br />
                  <button
                    className="btn btn-lg btn-success pull-xs-right"
                    type="submit"
                  >
                    Sign up
                  </button>
                </Form>
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
