import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";

const Comments = ({ slug }) => {
    const initialCommentList = JSON.parse(
        localStorage.getItem("commentlist") || "[]"
    );
    const [comment, setComment] = useState("");
    const [comments, setComments] = useState(initialCommentList);
    const [user, setUser] = useState({});

    useEffect(() => {
        const token = JSON.parse(localStorage.getItem("token"));
        axios
            .get("https://api.realworld.io/api/user", {
                headers: {
                    accept: "application/json",
                    Authorization: "Bearer " + token,
                },
            })
            .then((response) => {
                setUser(response.data.user);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);

    console.log(user.timestampFormatted);

    const handleCommentChange = (event) => {
        setComment(event.target.value);
    };

    const handlePostComment = () => {
        // Add the new comment to the comments list
        const newComment = {
            username: user.username,
            image: user.image,
            content: comment,
            slug: slug,
            idUser: user.id,
            id: new Date().getTime(),
            timestampFormatted: new Date(Date.now())
                .toLocaleDateString("vi-VN")
                .concat(" ", new Date(Date.now()).toLocaleTimeString("vi-VN")),
        };

        const newCommentlist = [...comments, newComment];
        setComments(newCommentlist);
        localStorage.setItem("commentlist", JSON.stringify(newCommentlist));
        setComment("");
    };

    const handleKeyPress = (e) => {
        if (e.key === "Enter") {
            handlePostComment();
        }
    };

    const handleDeleteComment = (commentId) => {
        const updatedComments = comments.filter((c) => c.id !== commentId);
        setComments(updatedComments);
        localStorage.setItem("commentlist", JSON.stringify(updatedComments));
    };
    return (
        <div>
            <div className="row mt-5">
                <div className="col-xs-12 col-md-8 offset-md-2">
                    <form className="card comment-form">
                        <div className="card-block">
                            <textarea
                                name="comment"
                                className="form-control"
                                placeholder="Write a comment..."
                                rows="3"
                                value={comment}
                                onChange={handleCommentChange}
                                onKeyPress={handleKeyPress}
                            ></textarea>
                        </div>
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "space-between",
                            }}
                            className="card-footer"
                        >
                            <img
                                src="https://api.realworld.io/images/smiley-cyrus.jpeg"
                                className="comment-author-img"
                                style={{
                                    borderRadius: "50%",
                                    height: "40px",
                                    marginRight: "auto",
                                }}
                            />
                            <button
                                style={{
                                    marginLeft: "auto",
                                    color: "white",
                                    fontWeight: "bold",
                                }}
                                type="button"
                                className="btn btn-sm btn-success"
                                onClick={handlePostComment}
                            >
                                Post Comments
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div className="row mt-3 mb-5">
                <div className="col-xs-12 col-md-8 offset-md-2">
                    <div className="card">
                        <div>
                            {comments
                                .filter((com) => com.slug === slug)
                                .map((c, index) => (
                                    <div
                                        style={{
                                            padding: "20px",
                                            marginBottom: "20px",
                                        }}
                                        key={index}
                                        className="card-text"
                                    >
                                        <div
                                            style={{
                                                border: "2px solid #ccc",
                                                padding: "20px",
                                            }}
                                        >
                                            {c.content}
                                        </div>

                                        <div
                                            style={{
                                                display: "flex",
                                                alignItems: "center",
                                                background:
                                                    "rgb(219, 217, 217)",
                                                minHeight: "50px",
                                            }}
                                        >
                                            <img
                                                src={c.image}
                                                className="comment-author-img"
                                                style={{
                                                    borderRadius: "50%",
                                                    height: "25px",
                                                    marginRight: "5px",
                                                    marginLeft: "10px",
                                                }}
                                            />

                                            <span
                                                style={{
                                                    color: "green",
                                                    display: "block",
                                                    marginRight: "15px",
                                                }}
                                            >
                                                {c.username}
                                            </span>
                                            <span
                                                style={{
                                                    color: "grey",
                                                    fontSize: "12px",
                                                }}
                                            >
                                                {c.timestampFormatted}
                                            </span>
                                            {user.id === c.idUser && (
                                                <div
                                                    style={{
                                                        // background: "silver",
                                                        padding: "10px",
                                                        marginLeft: "auto",
                                                    }}
                                                >
                                                    <button
                                                        style={{
                                                            background: "none",
                                                            border: "none",
                                                            color: "red",
                                                            cursor: "pointer",
                                                        }}
                                                        onClick={() =>
                                                            handleDeleteComment(
                                                                c.id
                                                            )
                                                        }
                                                    >
                                                        <FontAwesomeIcon
                                                            icon={faTrash}
                                                        />
                                                    </button>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Comments;
