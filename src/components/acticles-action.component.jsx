import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { deleteArticle } from "../services/articles.service";

export default function ArticleActions({ article }) {
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();

  async function handleDelete() {
    setIsLoading(true);
    const response = await deleteArticle(article.slug);
    setIsLoading(false);

    if (response.ok) {
      navigate("/");
      return;
    }
  }

    return (
      <>
        <Link
          className="btn btn-outline-secondary btn-sm"
          ui-sref="app.editor({ slug: $ctrl.article.slug })"
          to={"/editor/" + article.slug}
        >
          <i className="ion-edit"></i> Edit Article
        </Link>
        <button
          className="btn btn-outline-danger btn-sm"
          onClick={handleDelete}
          disabled={isLoading}
        >
          <i className="ion-trash-a"></i> Delete Article
        </button>
      </>
    );

}
