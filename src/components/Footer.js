import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <footer className=" bg-light text-white text-center py-3">
      <div className="container-fluid">
        <p className="m-0 text-black">
          <Link to="/" className="text-decoration-none text-success fw-bold">Conduit</Link>
          - An interactive learning project from{' '}
          <a href="https://thinkster.io/" className="text-decoration-none text-success" target="_blank" rel="noopener noreferrer">Thinkster</a>.
          Code & design licensed under MIT.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
