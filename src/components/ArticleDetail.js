import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Link } from "react-router-dom";
import Comments from "./Comments";

const ArticleDetail = () => {
    const [article, setArticle] = useState(null);
    const { slug } = useParams();
    const navigate = useNavigate();
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const token = JSON.parse(localStorage.getItem("token"));

    useEffect(() => {
        const initialToken = localStorage.getItem('token');
        const isLoggedIn = !!initialToken;
        setIsLoggedIn(isLoggedIn);
        fetchArticle();
    }, [slug]);

    const fetchArticle = async () => {
        try {
            const response = await fetch(`https://api.realworld.io/api/articles/${slug}`);
            if (!response.ok) {
                throw new Error('Article fetch failed');
            }
            const data = await response.json();
            const storedFavorites = JSON.parse(localStorage.getItem('favorites')) || {};
            const favoritedData = storedFavorites[data.article.slug];
            if (favoritedData) {
                data.article.favorited = favoritedData.favorited;
                data.article.favoritesCount = favoritedData.favoritesCount;
            }
            setArticle(data.article);
        } catch (error) {
            console.error(error);
        }
    };

    const toggleFavorite = async () => {
        if (!isLoggedIn) {
            navigate('/login');
            return;
        }
        const method = article.favorited ? 'DELETE' : 'POST';
        try {
            const response = await fetch(`https://api.realworld.io/api/articles/${article.slug}/favorite`, {
                method: method,
                headers: {
                    Authorization: `Token ${localStorage.getItem('token')}`
                }
            });
            const data = await response.json();

            const updatedArticle = {
                ...article,
                favorited: !article.favorited,
                favoritesCount: article.favorited ? article.favoritesCount - 1 : article.favoritesCount + 1
            };
            setArticle(updatedArticle);

            updateFavoritesInStorage(updatedArticle);
        } catch (error) {
            console.error(`Error ${(method === 'POST' ? 'favoriting' : 'unfavoriting')} article:`, error);
        }
    };

    const updateFavoritesInStorage = (article) => {
        const favorites = JSON.parse(localStorage.getItem('favorites')) || {};
        favorites[article.slug] = {
            favorited: article.favorited,
            favoritesCount: article.favoritesCount
        };
        localStorage.setItem('favorites', JSON.stringify(favorites));
    };


    if (!article) {
        return <div>Loading...</div>;
    }

    return (
        <div style={{ overflowX: 'hidden' }}>
            <div style={{ backgroundColor: '#333', color: 'white', padding: '2rem' }}>
                <div className="container">
                    <h1 style={{ fontWeight: 'bold' }}>{article.title}</h1>
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <img
                                src={article.author.image}
                                alt={article.author.username}
                                style={{ width: '50px', height: '50px', borderRadius: '50%', marginRight: '10px' }}
                            />
                            <div>
                                <a href=".#" style={{ color: 'white', fontWeight: 'bold', textDecoration: 'none' }}>
                                    {article.author.username}
                                </a>
                                <p style={{ margin: '5px 0', color: 'white' }}>
                                    {new Date(article.createdAt).toDateString()}
                                </p>
                            </div>
                        </div>
                        <div>
                            <button
                                className={`btn ${article.favorited ? 'btn-success' : 'btn-outline-success'}`}
                                onClick={() => toggleFavorite(article)}
                            >
                                <i className={`fa-heart ${article.favorited ? 'fas' : 'far'}`}></i>&nbsp;
                                {article.favorited ? `Unfavorite Article (${article.favoritesCount})` : `Favorite Article (${article.favoritesCount})`}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container border-bottom pb-4"
                style={{ marginTop: "2rem" }}>
                <div className="row">
                    <div className="col-md-12">
                        <p style={{ whiteSpace: 'pre-wrap' }}>{article.body}</p>
                    </div>
                </div>
                <div className="tag-list mt-2">
                    {article.tagList.map(tag => (
                        <span key={tag} className="badge bg-light text-dark rounded p-1 mr-2">{tag}</span>
                    ))}
                </div>
            </div>
            {token ? (
                <Comments slug={slug} />
            ) : (
                <div className="row mt-3">
                    <div className="col-xs-12 col-md-8 offset-md-2">
                        <p>
                            <Link className="text-success" to="/login">
                                Sign in
                            </Link>
                            &nbsp; or &nbsp;
                            <Link className="text-success" to="/register">
                                Sign up
                            </Link>
                            &nbsp; to add comments on this article.
                        </p>
                    </div>
                </div>
            )}
        </div>
    );
};

export default ArticleDetail;

