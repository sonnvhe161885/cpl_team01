export default function FormattedDate({ date }) {
  const formattedDate = new Intl.DateTimeFormat("en-US", {
    year: "numeric",
    month: "long",
    day: "numeric",
  }).format(new Date(date));

  return <span className="date">{formattedDate}</span>;
}
