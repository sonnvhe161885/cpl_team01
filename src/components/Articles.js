import React, { useState, useEffect } from 'react';
import { NavLink, Link, useNavigate } from 'react-router-dom';

const Articles = () => {
    const [articles, setArticles] = useState([]);
    const [activeFeed, setActiveFeed] = useState('global');
    const [tags, setTags] = useState([]);
    const [selectedTag, setSelectedTag] = useState(null);
    const [offset, setOffset] = useState(0);
    const [totalArticles, setTotalArticles] = useState(0);
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        const initialToken = localStorage.getItem('token');
        const isLoggedIn = !!initialToken;
        setIsLoggedIn(isLoggedIn);

        fetchArticles(activeFeed, selectedTag);
        fetchTags();
    }, [offset, activeFeed, selectedTag]);
    useEffect(() => {
        const storedFavorites = JSON.parse(localStorage.getItem('favorites')) || {};
        setArticles(articles => articles.map(article => ({
            ...article,
            favorited: storedFavorites[article.slug]?.favorited || article.favorited,
            favoritesCount: storedFavorites[article.slug]?.favoritesCount || article.favoritesCount
        })));
    }, [articles]);

    const fetchArticles = async (feedType, tag = '') => {
        try {
            let url = `https://api.realworld.io/api/articles?limit=10&offset=${offset}`;
            if (feedType === 'your') {
                url = `https://api.realworld.io/api/articles/feed?limit=10&offset=${offset}`;
            }
            if (tag) {
                url += `&tag=${encodeURIComponent(tag)}`;
            }
            const response = await fetch(url);
            const data = await response.json();
            setArticles(data.articles || []);
            setTotalArticles(Math.max(0, data.articlesCount || 0));
            setActiveFeed(feedType);
            setSelectedTag(tag);
        } catch (error) {
            console.error('Error fetching articles:', error);
            setArticles([]);
        }
    };

    const fetchTags = async () => {
        try {
            const response = await fetch('https://api.realworld.io/api/tags');
            const data = await response.json();
            setTags(data.tags);
        } catch (error) {
            console.error('Error fetching tags:', error);
        }
    };

    const handlePageChange = (newPage) => {
        const newOffset = newPage * 10;
        setOffset(newOffset);
    };
    const toggleFavorite = async (article) => {
        if (!isLoggedIn) {
            navigate('/login');
            return;
        }
        const method = article.favorited ? 'DELETE' : 'POST';
        try {
            const response = await fetch(`https://api.realworld.io/api/articles/${article.slug}/favorite`, {
                method: method,
                headers: {
                    Authorization: `Token ${localStorage.getItem('token')}`
                }
            });
            const data = await response.json();

            const updatedArticles = articles.map(a => a.slug === article.slug ? { ...a, favorited: !a.favorited, favoritesCount: a.favorited ? a.favoritesCount - 1 : a.favoritesCount + 1 } : a);
            setArticles(updatedArticles);
            saveFavoritesToStorage(updatedArticles);
        } catch (error) {
            console.error(`Error ${(method === 'POST' ? 'favoriting' : 'unfavoriting')} article:`, error);
        }
    };

    const saveFavoritesToStorage = (articles) => {
        const favorites = articles.reduce((acc, article) => {
            acc[article.slug] = { favorited: article.favorited, favoritesCount: article.favoritesCount };
            return acc;
        }, {});
        localStorage.setItem('favorites', JSON.stringify(favorites));
    };

    const totalPages = Math.ceil(Math.max(0, totalArticles) / 10);
    const activeTabStyle = {
        paddingRight: '20px',
        color: '#5cb85c',
        borderBottom: '2px solid #5cb85c',
        paddingBottom: '10px',
        textDecoration: 'none'

    };
    const tabStyle = {
        paddingRight: '20px',
        textDecoration: 'none',
        color: "black"
    };

    return (
        <div className="container mt-5">
            <div className="row">
                <div className="col-md-9">
                    <div className="nav nav-pills mb-3">
                        {isLoggedIn && (
                            <NavLink

                                style={activeFeed === 'your' && !selectedTag ? activeTabStyle : tabStyle}
                                onClick={() => {
                                    setActiveFeed('your');
                                    setSelectedTag(null);
                                    setOffset(0);
                                }}
                            >
                                Your Feed
                            </NavLink>
                        )}
                        <NavLink
                            style={(!isLoggedIn || activeFeed === 'global') && !selectedTag ? activeTabStyle : tabStyle}
                            onClick={() => {
                                setActiveFeed('global');
                                setSelectedTag(null);
                                setOffset(0);
                            }}
                        >
                            Global Feed
                        </NavLink>
                        {selectedTag && (
                            <span
                                style={selectedTag ? activeTabStyle : tabStyle}
                            >
                                #{selectedTag}
                            </span>
                        )}
                    </div>

                    {articles.length === 0 && <div>No articles are here... yet.</div>}

                    {articles.map((article) => (
                        <div key={article.slug} className="card mb-3">
                            <div className="card-header d-flex align-items-center">
                                <img
                                    src={article.author.image}
                                    alt={article.author.username}
                                    className="rounded-circle mr-3"
                                    style={{ width: '50px', height: '50px' }}
                                />
                                <div>
                                    <div

                                        className="font-weight-bold text-success"
                                    >
                                        {article.author.username}
                                    </div>

                                    <div className="text-muted">
                                        {new Date(article.createdAt).toDateString()}
                                    </div>
                                    <button className={`btn ${article.favorited ? 'btn-success' : 'btn-outline-success'}`} onClick={() => toggleFavorite(article)}>
                                        {article.favoritesCount} <i className="fa-solid fa-heart"></i>
                                    </button>

                                </div>
                            </div>
                            <div className="card-body">
                                <Link
                                    to={`/article/${article.slug}`}
                                    className="text-dark text-decoration-none"
                                >
                                    <h5 className="card-title">{article.title}</h5>
                                    <p className="card-text">{article.description}</p>
                                    <span className="text-success">Read more...</span>
                                    <div className="tag-list mt-2">
                                        {article.tagList.map((tag) => (
                                            <span
                                                key={tag}
                                                className="badge bg-light text-dark rounded p-1 mr-2"
                                            >
                                                {tag}
                                            </span>
                                        ))}
                                    </div>
                                </Link>
                            </div>
                        </div>
                    ))}

                    {totalPages > 0 && (
                        <nav aria-label="Page navigation example">
                            <ul className="pagination">
                                {[...Array(totalPages).keys()].map(pageNumber => (
                                    <li key={pageNumber} className={`page-item ${offset / 10 === pageNumber ? 'active' : ''}`}>
                                        <button className="page-link" onClick={() => handlePageChange(pageNumber)}>
                                            {pageNumber + 1}
                                        </button>
                                    </li>
                                ))}
                            </ul>
                        </nav>
                    )}
                </div>
                <div className="col-md-3 ">
                    <div className="card bg-light">
                        <div className="card-body">
                            <h5 className="card-title">Popular Tags</h5>
                            <div className="d-flex flex-wrap">
                                {tags.map((tag, index) => (
                                    <button
                                        key={index}
                                        className="badge bg-secondary text-white m-1 border-0"
                                        onClick={() => {
                                            setSelectedTag(tag);
                                            setActiveFeed('global');
                                            setOffset(0);
                                        }}
                                    >
                                        {tag}
                                    </button>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Articles;
