import React, { useState, useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useNavigate, Link } from "react-router-dom";
import axios from "axios";

function Login() {
  const nav = useNavigate();

  const [token, setToken] = useState("");


  const validationShema = Yup.object().shape({
    email: Yup.string().required("Email is required"),
    password: Yup.string().required("Password is required"),
  });
  
  const handleLogin = (user) => {
  
    axios
      .post(
        "https://api.realworld.io/api/users/login",
        {
          user: user,
        },
        {
          "Content-Type": "application/json",
        }
      )
      .then((res) => {
        const newToken = res.data.user.token;
        
        localStorage.setItem("token", JSON.stringify(newToken));
        
        nav("/");
        
      });
      
    
  };
  useEffect(() => {
    const initialToken = localStorage.getItem("token");
    const parsedToken = JSON.parse(initialToken);
    if (initialToken) {
      setToken(initialToken);
      
    }
    console.log("Tokn",token);
  }, []);

  

  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center text-center">Sign in</h1>
            <p className="text-xs-center text-center">
              <Link to="/register">Need an account?</Link>
            </p>
            <Formik
              initialValues={{ email: "", password: "" }}
              onSubmit={(values) => handleLogin(values)}
              validationSchema={validationShema}
            >
              <Form>
                <ErrorMessage
                  className="text-center text-danger"
                  name="email"
                  component={"div"}
                />
                <br />
                <ErrorMessage
                  className="text-center text-danger"
                  name="password"
                  component={"div"}
                />
                <br />
                <Field
                  className="form-control form-control-lg"
                  name="email"
                  type="email"
                  placeholder="Email"
                />
                <br />
                <Field
                  className="form-control form-control-lg"
                  name="password"
                  type="password"
                  placeholder="Password"
                />
                <br />
                <button
                  className="btn btn-lg btn-success pull-xs-right"
                  type="submit"
                >
                  Sign in
                </button>
              </Form>
            </Formik>
          </div>
        </div>
      </div>
      <div></div>
    </div>
  );
}

export default Login;
const TOKEN_KEY = "token";

