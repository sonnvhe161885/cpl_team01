import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Formik, Form, Field, ErrorMessage, useField } from 'formik';
import * as Yup from 'yup';
import { useNavigate } from 'react-router';

function UpdateProfile() {
    const [user, setUser] = useState({});
    const token = JSON.parse(localStorage.getItem('token') || '{}');
    const [loading, setLoading] = useState(true); // State to track loading
    const nav = useNavigate();


    const MyTextArea = ({ label, ...props }) => {
        const [field, meta] = useField(props);
        return (
            <>
                <textarea className="text-area" {...field} {...props} />
            </>
        );
    };
    const validationSchema = Yup.object().shape({
        image: Yup.string(),
        username: Yup.string().required('Username is required'),
        bio: Yup.string(),
        email: Yup.string().email('Invalid email address').required('Email is required'),
        password: Yup.string(),
    });

//get user show in the form
    useEffect(() => {
        if (!token) {
            console.log('Token not found in localStorage');
            return;
        }
        axios.get('https://api.realworld.io/api/user', {
            headers: {
                'accept': 'application/json',
                Authorization: "Bearer " + token
            }
        })
            .then((response) => {
                setUser(response.data.user);
                setLoading(false);
            })
            .catch((error) => {
                console.log(error);
            });
    }, [])

    //handle update form
    const handleUpdate = async (values) => {
        const updateUser = {
            user: {
                image: values.image,
                username: values.username,
                bio: values.bio,
                email: values.email,
                password: values.password,
            },
        };

        try {
            const res = await axios.put('https://api.realworld.io/api/user', updateUser, {
                headers:
                {
                    'accept': 'application/json',
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
            console.log(res.data.user);
            alert('update successfull');
        } catch (error) {

        }
    }
    const handleLogout = () => {
        localStorage.removeItem('token');
        setUser(null);
        nav('/login');
    }

    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <div className='setting-page'>
            <div className='container page'>
                <div className='row'>
                    <div className='col-md-6 offset-md-3 col-xs-12'>
                        <h1 className='text-xs-center text-center'>Your Settings</h1>
                        <Formik initialValues={{ image: user.image || "", username: user.username || "", bio: user.bio || "", email: user.email || "", password: "" }}
                            onSubmit={(values) => handleUpdate(values)}
                            validationSchema={validationSchema}
                        >
                            {({ values }) => (
                                <Form>
                                    <ErrorMessage className='text-center text-danger' name="image" component={"div"} />
                                    <ErrorMessage className='text-center text-danger' name="username" component={"div"} />
                                    <ErrorMessage className='text-center text-danger' name="email" component={"div"} />
                                    <ErrorMessage className='text-center text-danger' name="password" component={"div"} />
                                    <Field className='form-control form-control-lg' id='image' name='image' type='text' placeholder='Image' value={values.image} /><br />
                                    <Field className='form-control form-control-lg' id='username' name='username' type='text' placeholder='User Name' value={values.username} /><br />
                                    <MyTextArea
                                        className='form-control form-control-lg'
                                        id="bio"
                                        name="bio"
                                        rows="8"
                                        placeholder="Short bio about you"
                                    /><br/>
                                    <Field className='form-control form-control-lg' name='email' type='email' placeholder='Email' /><br />
                                    <Field className='form-control form-control-lg' name='password' type='text' placeholder='Password' /><br />
                                    <button className="btn btn-lg btn-success pull-xs-right" type='submit'>Update Setting</button>
                                </Form>
                            )}
                        </Formik>
                        <button class="btn btn-outline-danger mt-5" onClick={() => handleLogout()}>Or click here to logout.</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default UpdateProfile;